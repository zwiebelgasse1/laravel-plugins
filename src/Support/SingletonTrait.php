<?php

namespace Zwiebelgasse\LaravelPlugins\Support;

use PetrKnap\Php\Singleton\SingletonTrait as SingletonTraitBase;

trait SingletonTrait
{
    use SingletonTraitBase;

    /**
     * Returns instance, if instance does not exist then creates new one and returns it
     *
     * @return $this
     */
    public static function getInstance()
    {
        $self = get_called_class();
        if (! isset(self::$instances[$self])) {
            self::$instances[$self] = new $self(...func_get_args());
        }

        return self::$instances[$self];
    }
}
