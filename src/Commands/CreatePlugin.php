<?php

namespace Zwiebelgasse\LaravelPlugins\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Finder\SplFileInfo;

class CreatePlugin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-plugins:create {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup a base plugin';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $pluginName = $this->argument('name');
        $pluginCamelCaseName = Str::camel($pluginName);

        // config values
        $configPluginsDirectory = config('laravel-plugins.plugins_directory');
        $configBasePluginClassFileNameExtension = config('laravel-plugins.base_plugin_class_file_name_extension');

        /**@var Filesystem*/
        $fileSystem = $this->laravel->files;

        // copy from template/PluginName and replace "PluginName" with $pluginName
        $templatesDir = dirname(dirname(__DIR__)) . '/templates';
        $pluginTemplatesDir = $templatesDir . '/PluginName';
        $pluginDestinationDir = base_path("$configPluginsDirectory/$pluginName");

        if ($fileSystem->exists($pluginDestinationDir)) {
            $this->error("Plugin \"$pluginName\" already exists!");
            exit;
        }

        $fileSystem->copyDirectory($pluginTemplatesDir, $pluginDestinationDir);
        foreach ($fileSystem->allFiles($pluginDestinationDir) as $file) {
            /**@var SplFileInfo $file*/

            $basePluginClassName = $pluginName . $configBasePluginClassFileNameExtension;

            // replace namespace
            $fileSystem->replace(
                $file->getPathname(),
                str_replace('App\\Plugins', config('laravel-plugins.namespace'), $file->getContents())
            );

            // replace placeholder name
            $fileSystem->replace(
                $file->getPathname(),
                str_replace(
                    ['PluginName', 'pluginName', $pluginName . 'Plugin'],
                    [$pluginName, $pluginCamelCaseName, $basePluginClassName],
                    $file->getContents()
                )
            );

            // copy base class
            if (strpos($file->getPathname(), 'PluginNamePlugin') !== false) {
                $fileSystem->move(
                    $file->getPathname(),
                    str_replace('PluginNamePlugin', $basePluginClassName, $file->getPathname())
                );
            }
        }

        // set package.json scripts for plugin build
        $scriptPluginDev = "mix --mix-config $configPluginsDirectory/$pluginName/webpack.mix.js";
        $scriptPluginWatch = "mix watch --mix-config $configPluginsDirectory/$pluginName/webpack.mix.js";
        $scriptPluginProd = "mix --production --mix-config $configPluginsDirectory/$pluginName/webpack.mix.js";
        $package = $fileSystem->get('package.json');
        if (strpos($package, "plugin:$pluginCamelCaseName:dev") === false) {
            $endOfLineScript = PHP_EOL . "\t\t";
            $endOfLineScripts = PHP_EOL . "\t";
            $package = preg_replace(
                '/"scripts": {(.+?)}/s',
                '"scripts": ' .
                "{\$1,$endOfLineScript\"plugin:$pluginCamelCaseName:dev\": \"$scriptPluginDev\"" .
                '}',
                $package
            );
            $package = preg_replace(
                '/"scripts": {(.+?)}/s',
                '"scripts": ' .
                "{\$1,$endOfLineScript\"plugin:$pluginCamelCaseName:watch\": \"$scriptPluginWatch\"" .
                '}',
                $package
            );
            $package = preg_replace(
                '/"scripts": {(.+?)}/s',
                '"scripts": ' .
                "{\$1,$endOfLineScript\"plugin:$pluginCamelCaseName:prod\": \"$scriptPluginProd\"" .
                "$endOfLineScripts}",
                $package
            );
            $fileSystem->put('package.json', $package);
        }

        $this->info("Plugin \"$pluginName\" created at \"$pluginDestinationDir\"!");

        return 0;
    }
}
