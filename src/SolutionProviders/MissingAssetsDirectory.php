<?php

namespace Zwiebelgasse\LaravelPlugins\SolutionProviders;

use Facade\IgnitionContracts\BaseSolution;
use Facade\IgnitionContracts\HasSolutionsForThrowable;
use Illuminate\Support\Str;
use Throwable;

class MissingAssetsDirectory implements HasSolutionsForThrowable
{
    /**
     * @inheritDoc
     */
    public function canSolve(Throwable $throwable): bool
    {
        return Str::containsAll($throwable->getMessage(), ["Couldn't find", 'directory to symlink from']);
    }

    /**
     * @inheritDoc
     */
    public function getSolutions(Throwable $throwable): array
    {
        $pluginName = Str::camel($this->extractPluginNameFromMessage($throwable->getMessage()));

        return [
            BaseSolution::create("Missing plugin's assets directory")
                ->setSolutionDescription("Did you run `npm run plugin:$pluginName:dev`?"),
        ];
    }

    /**
     * Extracts the plugin name from the exception message
     */
    private function extractPluginNameFromMessage(string $message): string
    {
        $configPluginsDirectory = config('laravel-plugins.plugins_directory');

        preg_match("#/$configPluginsDirectory/(\w+)/#", $message, $matches);

        return $matches[1] ?? '';
    }
}
