<?php

namespace Zwiebelgasse\LaravelPlugins;

use Facade\Ignition\SolutionProviders\SolutionProviderRepository;
use Facade\IgnitionContracts\SolutionProviderRepository as SolutionProviderRepositoryContract;
use Illuminate\Support\ServiceProvider;
use Zwiebelgasse\LaravelPlugins\Commands\CreatePlugin;
use Zwiebelgasse\LaravelPlugins\SolutionProviders\MissingAssetsDirectory;

class PluginServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     */
    public function register(): void
    {
        // config
        $this->mergeConfigFrom(dirname(__DIR__) . '/config/config.php', 'laravel-plugins');

        // start PluginManager
        PluginManager::getInstance($this->app);

        // php artisan commands
        $this->commands([
            CreatePlugin::class,
        ]);

        // exception solutions @see vendor/facade/ignition/src/IgnitionServiceProvider.php:177
        $this->app->singleton(SolutionProviderRepositoryContract::class, function () {
            return new SolutionProviderRepository([
                MissingAssetsDirectory::class,
            ]);
        });
    }

    /**
     * Bootstrap services.
     *
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                dirname(__DIR__) . '/config/config.php' => config_path('laravel-plugins.php'),
            ], 'config');
        }

        // bind
        $this->app->bind('laravel-plugins', function ($app) {
            return PluginManager::getInstance($app);
        });

        // boot plugins
        $this->app['laravel-plugins']->bootPlugins();
    }
}
