<?php

namespace Zwiebelgasse\LaravelPlugins;

class PluginAction
{
    /**
     * @var array<array<string, callable, int>>
     */
    public static array $actions = [];

    /**
     * @var array<array<string, callable, int>>
     */
    public static array $filters = [];

    /**
     * Adds an action
     */
    public static function addAction(string $tag, callcable $functionToAdd, int $priority = 10): void
    {
        self::$actions[$tag][$priority][] = compact('tag', 'functionToAdd', 'priority');
    }

    /**
     * Adds a filter
     */
    public static function addFilter(string $tag, callable $functionToAdd, int $priority = 10): void
    {
        self::$filters[$tag][$priority][] = compact('tag', 'functionToAdd', 'priority');
    }

    /**
     * Runs an action
     */
    public static function doAction(string $tag, mixed ...$args): void
    {
        foreach (self::$actions[$tag] as $actions) {
            foreach ($actions as $action) {
                call_user_func_array($action['functionToAdd'], $args);
            }
        }
    }

    /**
     * Runs a filter
     */
    public static function applyFilter(string $tag, mixed $value, mixed ...$args): mixed
    {
        // set $value as first argument
        array_unshift($args, $value);

        foreach (self::$filters[$tag] as $filters) {
            foreach ($filters as $filter) {
                $value = call_user_func_array($filter['functionToAdd'], $args);
            }
        }

        return $value;
    }
}
