<?php

namespace Zwiebelgasse\LaravelPlugins;

use Composer\Semver\Semver;
use Illuminate\Contracts\Foundation\Application;
use Symfony\Component\Finder\Exception\DirectoryNotFoundException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Zwiebelgasse\LaravelPlugins\Exceptions\BootPluginException;
use Zwiebelgasse\LaravelPlugins\Exceptions\RegisterPluginException;
use Zwiebelgasse\LaravelPlugins\Support\SingletonTrait;

class PluginManager
{
    use SingletonTrait;

    private Application $app;

    protected string $pluginsDirectory;

    /**
     * @var array<Plugin>
     */
    protected array $plugins = [];

    protected bool $usePluginActivation;

    /**
     * @var array<string>
     */
    protected array $activePlugins = [];

    /**
     * PluginManager constructor.
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->pluginsDirectory = base_path(config('laravel-plugins.plugins_directory'));
        $this->usePluginActivation = config('laravel-plugins.use_plugin_activation', false);
        $this->activePlugins = config('laravel-plugins.active_plugins', []);

        if (file_exists($this->pluginsDirectory)) {
            $this->registerPlugins();
        }
    }

    /**
     * Creates a symlink for the assets to the public directory
     */
    public function createAssetsLink(string $plugin, string $path): void
    {
        $pluginPath = $this->pluginsDirectory . DIRECTORY_SEPARATOR . $plugin . DIRECTORY_SEPARATOR . $path;

        if (! file_exists($pluginPath)) {
            throw new DirectoryNotFoundException("Couldn't find $pluginPath directory to symlink from.");
        }

        $publicPath = public_path(config('laravel-plugins.plugins_assets_directory'));
        if (! file_exists($publicPath)) {
            $this->app['files']->makeDirectory($publicPath);
        }

        $pluginPublicPath = $publicPath . DIRECTORY_SEPARATOR . $plugin;
        if (! file_exists($pluginPublicPath)) {
            $this->app['files']->link($pluginPath, $pluginPublicPath);
        }
    }

    /**
     * Add all Plugins in plugin directory to app
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws RegisterPluginException
     */
    protected function registerPlugins(): void
    {
        $configBasePluginClassFileNameExtension = config('laravel-plugins.base_plugin_class_file_name_extension');

        $pluginsDirectories = $this->getPluginDirectories();
        foreach ($pluginsDirectories as $pluginDirectory) {
            /** @var SplFileInfo $pluginDirectory */
            $pluginDirectoryName = $pluginDirectory->getBasename();

            $pluginClass = $this->getPluginClassNameFromDirectory($pluginDirectoryName);

            if (! class_exists($pluginClass)) {
                throw new RegisterPluginException(
                    "Plugin \"$pluginDirectoryName\" needs a " .
                    "\"{$pluginDirectoryName}{$configBasePluginClassFileNameExtension}\" class."
                );
            }

            try {
                $plugin = $this->app->makeWith($pluginClass, [$this->app]);
            } catch (\ReflectionException $e) {
                throw new RegisterPluginException(
                    "Plugin \"$pluginDirectoryName\" could not be booted: \"{$e->getMessage()}\""
                );
            }

            if (! ($plugin instanceof Plugin)) {
                throw new RegisterPluginException(
                    "Plugin \"$pluginDirectoryName\" must extend the \"" . Plugin::class . "\" Base Class."
                );
            }

            $this->plugins[$pluginClass] = $plugin;
        }
    }

    /**
     * Boot all registered and potentially active plugins
     */
    public function bootPlugins(): void
    {
        $plugins = $this->getActivePlugins();
        foreach ($plugins as $pluginClass => $plugin) {
            // check dependencies
            foreach ($plugin->dependencies as $dependencyPluginClass => $constraint) {
                $dependency = $plugins[$dependencyPluginClass] ?? null;

                if (! $dependency) {
                    throw new BootPluginException(
                        "Plugin \"$pluginClass\" is missing the dependency \"$dependencyPluginClass\"."
                    );
                }

                // check version
                if (! Semver::satisfies($dependency->version, $constraint)) {
                    throw new BootPluginException(
                        "Plugin \"$pluginClass\"'s version constraint \"$constraint\"" .
                        "for the dependency \"$dependencyPluginClass\" doesn't match version \"$dependency->version\"."
                    );
                }
            }

            $plugin->boot();
        }
    }

    /**
     * Returns all plugin directories
     */
    public function getPluginDirectories(): Finder
    {
        return Finder::create()->in($this->pluginsDirectory)->directories()->depth(0);
    }

    /**
     * Returns the plugin class name based on the directory
     */
    public function getPluginClassNameFromDirectory(string $directory): string
    {
        return
            config('laravel-plugins.namespace') .
            "\\$directory\\$directory" .
            config('laravel-plugins.base_plugin_class_file_name_extension');
    }

    /**
     * Returns the active plugins
     *
     * Returns all plugins if usePluginActivation is false
     * Returns a filtered array if usePluginActivation is true
     *
     * @return array<Plugin>
     */
    public function getActivePlugins(): array
    {
        return $this->usePluginActivation ?
            array_filter(
                $this->plugins,
                fn ($plugin, $pluginClass) => in_array($pluginClass, $this->activePlugins),
                ARRAY_FILTER_USE_BOTH
            ) :
            $this->plugins;
    }

    /**
     * @param array<string> $pluginClasses
     */
    public function setActivePlugins(array $pluginClasses): void
    {
        $this->activePlugins = $pluginClasses;
    }

    /**
     * @param string $name The Plugin's fully qualified classname
     *                     App\Plugins\PluginName\PluginNamePlugin::class
     */
    public function getPlugin(string $name): Plugin
    {
        return $this->plugins[$name];
    }

    /**
     * @return array<Plugin>
     */
    public function getPlugins(): array
    {
        return $this->plugins;
    }
}
