<?php

namespace Zwiebelgasse\LaravelPlugins;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Str;
use Illuminate\Translation\Translator;

abstract class Plugin
{
    protected Application $app;

    /**
     * The Plugin Name.
     */
    public string $name;

    /**
     * A description of the plugin.
     */
    public string $description;

    /**
     * The version of the plugin.
     */
    public string $version;

    /**
     * The dependencies to look for before loading the plugin
     *
     * @var array<string>
     */
    public array $dependencies = [];

    /**
     * @var $this
     */
    private $reflector = null;

    /**
     * Plugin constructor.
     */
    public function __construct(Application $app)
    {
        $this->app = $app;

        $this->checkPluginMeta();
    }

    /**
     * Boot the plugin.
     */
    abstract public function boot(): void;

    /**
     * Check for empty plugin meta.
     *
     * @throws \InvalidArgumentException
     */
    private function checkPluginMeta(): void
    {
        if (! $this->name) {
            throw new \InvalidArgumentException('Missing Plugin name.');
        }
        if (! $this->description) {
            throw new \InvalidArgumentException('Missing Plugin description.');
        }
        if (! $this->version) {
            throw new \InvalidArgumentException('Missing Plugin version.');
        }
    }

    /**
     * Returns the view namespace in a camel case format based off
     * the plugins class name, with plugin stripped off the end.
     *
     * Eg: ArticlesPlugin will be accessible through 'plugin:articles::<view name>'
     */
    protected function getViewNamespace(): string
    {
        return 'plugin:' . Str::camel(
            mb_substr(
                get_called_class(),
                strrpos(get_called_class(), '\\') + 1,
                -strlen(config('laravel-plugins.base_plugin_class_file_name_extension'))
            )
        );
    }

    /**
     * Add a view namespace for this plugin.
     * Eg: view("plugin:articles::{view_name}")
     */
    protected function enableViews(string $path = 'views'): void
    {
        $this->app['view']->addNamespace(
            $this->getViewNamespace(),
            $this->getPluginPath() . DIRECTORY_SEPARATOR . $path
        );
    }

    /**
     * Enable routes for this plugin.
     *
     * @param array<string>|string $middleware
     */
    protected function enableRoutes(string $path = 'routes.php', $middleware = 'web'): void
    {
        $this->app->router->group(
            [
                'namespace' => $this->getPluginControllerNamespace(),
                'middleware' => $middleware,
            ],
            $this->getPluginPath() . DIRECTORY_SEPARATOR . $path
        );
    }

    /**
     * Register a database migration path for this plugin.
     *
     * @param  array<string>|string  $paths
     */
    protected function enableMigrations($paths = 'migrations'): void
    {
        $this->app->afterResolving('migrator', function ($migrator) use ($paths): void {
            foreach ((array)$paths as $path) {
                $migrator->path($this->getPluginPath() . DIRECTORY_SEPARATOR . $path);
            }
        });
    }

    /**
     * Add a translations namespace for this plugin.
     * Eg: __("plugin:articles::{trans_path}")
     */
    protected function enableTranslations(string $path = 'lang'): void
    {
        $this->app->afterResolving('translator', function (Translator $translator) use ($path): void {
            $translator->addNamespace(
                $this->getViewNamespace(),
                $this->getPluginPath() . DIRECTORY_SEPARATOR . $path
            );
        });
    }

    /**
     * Creates a link from the plugin's built assets
     * to the public folder
     */
    protected function enableAssets(string $path = 'build'): void
    {
        app()->afterResolving('laravel-plugins', function (PluginManager $pluginManager) use ($path): void {
            $pluginManager->createAssetsLink($this->name, $path);
        });
    }

    /**
     * Returns the full path to the plugin directory
     */
    public function getPluginPath(): string
    {
        $reflector = $this->getReflector();
        $fileName  = $reflector->getFileName();

        return dirname($fileName);
    }

    /**
     * Returns the plugin Controller namespace
     */
    protected function getPluginControllerNamespace(): string
    {
        $reflector = $this->getReflector();
        $baseDir   = str_replace($reflector->getShortName(), '', $reflector->getName());

        return $baseDir . 'Http\\Controllers';
    }

    /**
     * Sets and returns a reflection
     */
    private function getReflector(): \ReflectionClass
    {
        if (is_null($this->reflector)) {
            $this->reflector = new \ReflectionClass($this);
        }

        return $this->reflector;
    }

    /**
     * Returns a plugin view
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    protected function view(string $view)
    {
        return view($this->getViewNamespace() . '::' . $view);
    }

    /**
     * Returns the plugin instance
     */
    public static function getInstance(): self
    {
        return app('laravel-plugins')->getPlugin(get_called_class());
    }

    /**
     * Runs the mix helper for the current plugin
     *
     * @return \Illuminate\Support\HtmlString|string
     * @throws \Exception
     */
    public static function mix(string $path)
    {
        return mix(
            $path,
            config('laravel-plugins.plugins_assets_directory') . DIRECTORY_SEPARATOR . self::getInstance()->name
        );
    }
}
