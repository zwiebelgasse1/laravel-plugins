# laravel-plugins
Plugin system

```
composer require zwiebelgasse/laravel-plugins
```

## Usage
Add to `providers` array in `config/app.php`:
`\Zwiebelgasse\LaravelPlugins\PluginServiceProvider::class,`


### Using plugin activation
If you don't want plugins to be enabled by default, you can set `use_plugin_activation` to `true` and either set `active_plugins` to a static list of plugins or create your own `PluginServiceProvider` that extends the libraries' `PluginServiceProvider` and sets the option pre-boot for dynamic active plugins:
```php
class PluginServiceProvider extends \Zwiebelgasse\LaravelPlugins\PluginServiceProvider
{
    public function boot()
    {
        app()->afterResolving('laravel-plugins', function (PluginManager $pluginManager) {
            $pluginManager->setActivePlugins([
                CoolNewPluginPlugin::class,
                CoolNewPluginDependencyPlugin::class,
            ]);
        });

        parent::boot();
    }
}
```


## Create a plugin
run
```
php artisan laravel-plugins:create CoolNewPlugin
```


### Views
Views automatically have a namespace (`"plugin:{name}"`), the name is defined by the main plugin class in a camel case format, with `Plugin` stripped from the end.  
To render a view use the namespace like this for example `view('plugin:coolNewPlugin::some.view.name');`


### Migrations
Keep in mind that migrations must follow the `yyyy_mm_dd_tttt_<name>.php` naming convention, for example `2020_10_10_111111_create_test_table.php` would be a valid migration.


### Translations
Translations are namespaced by the same namespace as the views.  
So to get a translation do `__('plugin:coolNewPlugin::some.translation')`


### Assets
To build and bundle the assets with the plugin you need to run a separate mix instance with `mix --mix-config app/Plugins/CoolNewPlugin/webpack.mix.js`  
The plugin's `build` directory is symlinked to `public/laravel-plugins/CoolNewPlugin`.  
To use the assets like you're used to you can use the Plugin's `mix()` method via `\App\Plugins\CoolNewPlugin\CoolNewPluginPlugin::mix('js/plugin.js')` in your views.


## Todo
- Use Facades to access methods on the plugins in views and elsewhere
