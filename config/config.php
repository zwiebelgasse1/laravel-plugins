<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Plugins Namespace
    |--------------------------------------------------------------------------
    |
    | The namespace to use for plugins.
    |
    */

    'namespace' => 'App\\Plugins',

    /*
    |--------------------------------------------------------------------------
    | Base Plugin Class File Name Extension
    |--------------------------------------------------------------------------
    |
    | The name suffix to look for in the plugin's base class.
    |
    */

    'base_plugin_class_file_name_extension' => 'Plugin',

    /*
    |--------------------------------------------------------------------------
    | Plugins Directory Path
    |--------------------------------------------------------------------------
    |
    | The plugins directory's folder path.
    |
    */

    'plugins_directory' => 'app/Plugins',

    /*
    |--------------------------------------------------------------------------
    | Plugins Assets Directory Name
    |--------------------------------------------------------------------------
    |
    | The directory name for the built assets in the "public" folder.
    |
    */

    'plugins_assets_directory' => 'laravel-plugins',

    /*
    |--------------------------------------------------------------------------
    | Use Plugin Activation
    |--------------------------------------------------------------------------
    |
    | Whether to check for active plugins before booting them.
    |
    */

    'use_plugin_activation' => false,

    /*
    |--------------------------------------------------------------------------
    | Active Plugins
    |--------------------------------------------------------------------------
    |
    | FQNs of active plugins.
    | Only applicable when "use_plugin_activation" is set to "true".
    |
    */

    'active_plugins' => [],

];
