<?php

namespace App\Plugins\PluginName;

use Zwiebelgasse\LaravelPlugins\Plugin;

class PluginNamePlugin extends Plugin
{
    public string $name = 'PluginName';
    public string $description = 'PluginName description';
    public string $version = '1.0.0';

    public function boot(): void
    {
        $this->enableViews();
        $this->enableRoutes();
        $this->enableMigrations();
        $this->enableTranslations();
        $this->enableAssets();
    }
}
