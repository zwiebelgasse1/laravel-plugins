<p class="test">
    {{ __('plugin:pluginName::test.test') }}
</p>

<link rel="stylesheet" href="{{ \App\Plugins\PluginName\PluginNamePlugin::mix('css/test.css') }}">
<script src="{{ \App\Plugins\PluginName\PluginNamePlugin::mix('js/test.js') }}"></script>
