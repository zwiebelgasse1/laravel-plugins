<?php

use App\Plugins\PluginName\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

Route::get('test', [TestController::class, 'index']);
